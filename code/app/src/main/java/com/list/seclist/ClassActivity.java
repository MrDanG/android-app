package com.list.seclist;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class ClassActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener{
    private ListView classNames;
    private ArrayAdapter<String> classNamesAdapter;

    private ContentManager contentManager;

    private String[] states = {"Löschen", "Umbenennen"};
    private AlertDialog.Builder dialog;
    private String selectedState;
    private String selectedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.setTitle(R.string.StartActivity_title);

        contentManager = (ContentManager) getApplicationContext();

        //ListView
        classNames = findViewById(R.id.class_names_ListView);
        //ArrayAdapter initialisieren. Daten aus Klassenliste lesen
        classNamesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                contentManager.getClassNames());
        //Adapter und ClickListener zuweisen
        classNames.setAdapter(classNamesAdapter);
        classNames.setOnItemClickListener(this);
        classNames.setOnItemLongClickListener(this);

        dialog = new AlertDialog.Builder(this);
        dialog.setItems(states, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                selectedState = states[which];
                longClickAction();
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                inputDialog("new").show();
            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();
        contentManager.writeData();
    }

    private AlertDialog inputDialog(final String operation){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View inputDialogView = LayoutInflater.from(this).inflate(R.layout.input_dialog,(ViewGroup) findViewById(android.R.id.content), false);
        final EditText inputDialogEditText = inputDialogView.findViewById(R.id.input_dialog_edit_text);
        inputDialogEditText.setHint(R.string.add_class_input_hint);
        builder.setView(inputDialogView);
        String positiveButtonText;
        if(operation.equals("new")){
            positiveButtonText = "Hinzufügen";
        }
        else{
            positiveButtonText = "Umbenennen";
        }
        builder.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogBox, int id){
                if(inputDialogEditText.getText().toString().matches("") == false){
                    if(operation.equals("new")){
                        addClassToList(inputDialogEditText.getText().toString());
                    }
                    else if(operation.equals("change")){
                        renameClass(inputDialogEditText.getText().toString());
                    }
                }
                dialogBox.dismiss();
            }
        });
        builder.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogBox, int id){
                dialogBox.cancel();
            }
        });

        return builder.create();
    }

    private void addClassToList(String name){
        classNamesAdapter.setNotifyOnChange(true);
        try{
            contentManager.addNewClass(name);
            classNamesAdapter.add(name);
            //sortieren
            classNamesAdapter.sort(new CustomComparator().getStringArrayListComparator());
        }
        catch (Exception e){
        }
    }

    private void renameClass(String name){
        classNamesAdapter.setNotifyOnChange(true);
        try{
            if(contentManager.renameClass(selectedItem, name) == -1){
                Toast.makeText(this, "Konnte nicht geändert werden.\nevtl. selber Name vorhanden.", Toast.LENGTH_LONG).show();
                return;
            }
            int pos = classNamesAdapter.getPosition(selectedItem);
            classNamesAdapter.remove(selectedItem);
            classNamesAdapter.insert(name, pos);
        }
        catch (Exception e){
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id){
        String tmpClassName = (String) classNames.getItemAtPosition(pos);

        final Intent i = new Intent(this, ListStudentActivity.class);

        i.putExtra("className", tmpClassName);

        startActivity(i);
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id){
        selectedItem = (String) classNames.getItemAtPosition(pos);
        dialog.show();
        return true;
    }

    private void longClickAction(){
        if(selectedState.equals("Löschen") && selectedItem != null){
            contentManager.deleteClass(selectedItem);
            classNamesAdapter.remove(selectedItem);
        }
        else if(selectedState.equals("Umbenennen") && selectedItem != null){
            inputDialog("change").show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.menu_info:
                Toast.makeText(this, "Langes Drücken auf einen Eintrag öffnet weitere Optionen.", Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
