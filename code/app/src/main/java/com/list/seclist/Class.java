package com.list.seclist;
import java.io.Serializable;
import java.util.Collections;
import java.util.ArrayList;
import java.util.TreeMap;

public class Class implements Serializable{
    private String name;
    private TreeMap<String, List> lists;
    private ArrayList<Student> students;

    Class(String name_){
        name = name_;
        lists = new TreeMap<>();
        students = new ArrayList<>();
        ContentManager.dataChanged();
    }

    ArrayList<String> getListsNames(){
        ArrayList<String> tmpNames = new ArrayList<>();
        //wie foreach über lists und alle keys einzeln hinzufügen
        tmpNames.addAll(lists.keySet());
        return sortStringLists(tmpNames);
    }
    List getList(String name_){
        return this.lists.get(name_);
    }

    int addNewList(String name_){
        if(name_ != null){
            if(lists.containsKey(name_)){
                return -1;
            }
            else{
                List tmpList = new List(name_);
                this.lists.put(name_, tmpList);
                ContentManager.dataChanged();
                return 0;
            }
        }
        return -1;
    }
    void deleteList(String name_){
        try{
            this.lists.remove(name_);
            ContentManager.dataChanged();
        }
        catch(Exception ignored){
        }
    }

    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
        ContentManager.dataChanged();
    }

    Iterable getStudents() {
        return students;
    }
    ArrayList<String> getStudentsNames(){
        ArrayList<String> tmpNames = new ArrayList<>();
        for(Student student : students){
            tmpNames.add(student.getName());
        }
        return sortStringLists(tmpNames);
    }
    int addStudent(String name_){
        for (Student s : students){
            if(s.getName().equals(name_)){
                return -1;
            }
        }
        Student tmpStudent = new Student(name_);
        this.students.add(tmpStudent);
        Collections.sort(students, new CustomComparator().getStudentArrayListComparator());
        ContentManager.dataChanged();
        return 0;
    }
    void deleteStudent(String name_){
        try{
            for (Student s : students){
                if(s.getName().equals(name_)){
                    this.students.remove(s);
                }
            }
            //wenn ein student gelöscht wird
            //muss auch die verknüpfung zu den aufgaben gelöscht werden
            //ansonsten läuft der speicher über
            for(List s : lists.values()){
                s.deleteEntryToStudent(name_);
            }
            ContentManager.dataChanged();
        }
        catch(Exception ignored){
        }
    }
    public int renameList(String previousName, String newName_){
        if(newName_ != null && previousName != null){
            if(lists.containsKey(newName_) == false){
                List tmpList = lists.remove(previousName);
                tmpList.setName(newName_);
                lists.put(newName_, tmpList);
                return 0;
            }
        }
        return -1;
    }
    public int renameStudent(String previousName, String newName_){
        if(newName_ != null && previousName != null){
            for(Student s : students){
               if(s.getName().equals(previousName)){
                   s.setName(newName_);
                   return 0;
               }
            }
        }
        return -1;
    }
    int getNumberOfStudents(){
        return this.students.size();
    }

    private ArrayList<String> sortStringLists(ArrayList<String> tmpList){
        Collections.sort(tmpList, String.CASE_INSENSITIVE_ORDER);
        return tmpList;
    }

}
