package com.list.seclist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class TabLists extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener{
    private ListView ListNames;
    private ArrayAdapter<String> ListNamesAdapter;

    private ListStudentActivity relatedActivity;
    private Class currentClass;

    private String[] states = {"Löschen", "Umbenennen"};
    private AlertDialog.Builder dialog;
    private String selectedState;
    private String selectedItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.tab_list, container, false);

        if(getActivity() instanceof ListStudentActivity){
            relatedActivity = (ListStudentActivity) getActivity();
            currentClass = relatedActivity.getCurrentClass();
        }

        //ListView
        ListNames = view.findViewById(R.id.subject_names_ListView);
        //ArrayAdapter initialisieren. Daten aus entsprechender Klasse lesen
        ListNamesAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1,
                currentClass.getListsNames());
        //Adapter und ClickListener zuweisen
        ListNames.setAdapter(ListNamesAdapter);
        ListNames.setOnItemClickListener(this);
        ListNames.setOnItemLongClickListener(this);

        dialog = new AlertDialog.Builder(relatedActivity);
        dialog.setItems(states, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                selectedState = states[which];
                longClickAction();
            }
        });

        return view;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id){
        String tmpSubjectName = (String) ListNames.getItemAtPosition(pos);

        final Intent i = new Intent(relatedActivity, ContentListsActivity.class);

        i.putExtra("subject", currentClass.getList(tmpSubjectName).getName());
        i.putExtra("class", currentClass.getName());

        startActivity(i);
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id){
        selectedItem = (String) ListNames.getItemAtPosition(pos);
        dialog.show();
        return true;
    }

    private void longClickAction(){
        if(selectedState.equals("Löschen") && selectedItem != null){
            currentClass.deleteList(selectedItem);
            ListNamesAdapter.remove(selectedItem);
        }
        else if(selectedState.equals("Umbenennen") && selectedItem != null){
            inputDialog().show();
        }
    }

    private AlertDialog inputDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(relatedActivity);
        View inputDialogView = LayoutInflater.from(relatedActivity).inflate(R.layout.input_dialog,(ViewGroup) relatedActivity.findViewById(android.R.id.content), false);
        final EditText inputDialogEditText = inputDialogView.findViewById(R.id.input_dialog_edit_text);
        inputDialogEditText.setHint(R.string.add_list_input_hint);
        builder.setView(inputDialogView);
        builder.setPositiveButton("Umbenennen", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogBox, int id){
                if(inputDialogEditText.getText().toString().matches("") == false){
                    ListNamesAdapter.setNotifyOnChange(true);
                    try{
                        if(currentClass.renameList(selectedItem, inputDialogEditText.getText().toString()) == -1){
                            Toast.makeText(relatedActivity, "Konnte nicht geändert werden.\nevtl. selber Name vorhanden.", Toast.LENGTH_LONG).show();
                            return;
                        }
                        int pos = ListNamesAdapter.getPosition(selectedItem);
                        ListNamesAdapter.remove(selectedItem);
                        ListNamesAdapter.insert(inputDialogEditText.getText().toString(), pos);
                    }
                    catch (Exception e){
                    }
                }
                dialogBox.dismiss();
            }
        });
        builder.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogBox, int id){
                dialogBox.cancel();
            }
        });

        return builder.create();
    }

    public void ListsChanged(String name_){
        ListNamesAdapter.setNotifyOnChange(true);
        if(name_ != null){
            ListNamesAdapter.add(name_);
            //sortieren
            ListNamesAdapter.sort(new CustomComparator().getStringArrayListComparator());
        }
    }
}
