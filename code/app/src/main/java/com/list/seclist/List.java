package com.list.seclist;

import java.io.Serializable;
import java.util.ArrayList;

public class List implements Serializable{
    private String name;
    private ArrayList<Entry> entries;

    public List(String name_){
        name = name_;
        entries = new ArrayList<>();
        ContentManager.dataChanged();
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name_){
        this.name = name_;
        ContentManager.dataChanged();
    }

    public void addExistingEntry(Entry entry_){
        if(entry_ != null){
            entries.add(entry_);
        }
    }
    public int addNewEntry(String name_){
        if(name_ != null){
            for (Entry t : entries){
                if(t.getName().equals(name_)){
                    return -1;
                }
            }
            Entry tmpEntry = new Entry(name_);
            this.entries.add(0, tmpEntry);
            ContentManager.dataChanged();
            return 0;
        }
        return -1;
    }
    public Iterable<Entry> getEntries(){
        return entries;
    }
    public void deleteEntryToStudent(String name_){
        try{
            for (Entry t : entries){
                t.deleteStatus(name_);
            }
            ContentManager.dataChanged();
        }
        catch(Exception e){
        }
    }
    public void deleteEntry(String name_){
        try{
            for (Entry t : entries){
                if(t.getName().equals(name_)){
                    entries.remove(t);
                }
            }
            ContentManager.dataChanged();
        }
        catch(Exception e){
        }
    }
    public int getNumberOfEntries(){
        return this.entries.size();
    }
    public void changeEntry(int index_, String name_, String status_){
        Entry tmpEntry = entries.get(index_);
        tmpEntry.setStatus(name_, status_);
        ContentManager.dataChanged();
    }
    public int renameEntry(String previousName, String newName_){
        if(newName_ != null && previousName != null){
            for(Entry t : entries){
                if(t.getName().equals(previousName)){
                    t.setName(newName_);
                    return 0;
                }
            }
        }
        return -1;
    }

}

