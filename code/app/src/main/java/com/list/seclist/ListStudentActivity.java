package com.list.seclist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

public class ListStudentActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private TabAdapter tabAdapter;
    private TabLayout.Tab selectedTab;

    private ContentManager contentManager;
    private Class currentClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_student);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        contentManager = (ContentManager) getApplicationContext();

        Intent i = getIntent();
        String currentClassName = i.getStringExtra("className");
        currentClass = contentManager.getClass(currentClassName);
        setTitle("Klasse - " + currentClass.getName());


        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //tabs hinzufügen mit addTab()
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        selectedTab = tabLayout.getTabAt(0);

        //viewPager initialisieren
        viewPager = (ViewPager) findViewById(R.id.pager);

        tabAdapter = new TabAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), this);
        viewPager.setAdapter(tabAdapter);

        tabLayout.setupWithViewPager(viewPager);

        //onTabSelectedListener zum swipen
        tabLayout.addOnTabSelectedListener(this);
        tabLayout.setupWithViewPager(viewPager);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                inputDialog().show();
            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();
        contentManager.writeData();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab){
        viewPager.setCurrentItem(tab.getPosition());
        selectedTab = tab;
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab){
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab){
    }

    private AlertDialog inputDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View inputDialogView = LayoutInflater.from(this).inflate(R.layout.input_dialog,(ViewGroup) findViewById(android.R.id.content), false);
        final EditText inputDialogEditText = inputDialogView.findViewById(R.id.input_dialog_edit_text);
        if(selectedTab.getText().equals(getText(R.string.tab_1_text))){
            inputDialogEditText.setHint(R.string.add_list_input_hint);
        }
        if(selectedTab.getText().equals(getText(R.string.tab_2_text))){
            inputDialogEditText.setHint(R.string.add_student_input_hint);
        }
        builder.setView(inputDialogView);
        builder.setPositiveButton("Hinzufügen", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogBox, int id){
                if(inputDialogEditText.getText().toString().matches("") == false){
                    //entspricht tab "Listen"
                    if(selectedTab.getText().equals(getText(R.string.tab_1_text))){
                        addSubjectToList(inputDialogEditText.getText().toString());
                    }
                    //entspricht Tab "Schüler"
                    else if(selectedTab.getText().equals(getText(R.string.tab_2_text))){
                        addStudentToList(inputDialogEditText.getText().toString());
                    }
                }
                dialogBox.dismiss();
            }
        });
        builder.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogBox, int id){
                dialogBox.cancel();
            }
        });

        return builder.create();
    }

    private void addSubjectToList(String name_){
        try{
            if(currentClass.addNewList(name_) == -1){
                Toast.makeText(this, "Konnte nicht hinzugefügt werden.\nevtl. selber Name vorhanden.", Toast.LENGTH_LONG).show();
                return;
            }
            tabAdapter.addList(name_);
        }
        catch (Exception e){
        }
    }

    private void addStudentToList(String name_){
        try{
            if(currentClass.addStudent(name_) == -1){
                Toast.makeText(this, "Konnte nicht hinzugefügt werden.\nevtl. selber Name vorhanden.", Toast.LENGTH_LONG).show();
                return;
            }
            tabAdapter.addStudent(name_);
        }
        catch (Exception e){
        }
    }

    public Class getCurrentClass(){
        return currentClass;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_info:
                Toast.makeText(this, "Langes Drücken auf einen Eintrag öffnet weitere Optionen.", Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
