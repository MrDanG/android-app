package com.list.seclist;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

public class ContentListsActivity extends AppCompatActivity {
    private Class currentClass;
    private List currentList;
    private ContentManager contentManager;
    private RecyclerView studentsTasksTable;
    private StudentsEntriesAdapter studentsEntriesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_lists);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        contentManager = (ContentManager) getApplicationContext();

        Intent i = getIntent();
        String currentClassName = i.getStringExtra("class");
        currentClass = contentManager.getClass(currentClassName);
        String currentSubjectName = i.getStringExtra("subject");
        currentList = currentClass.getList(currentSubjectName);

        setTitle("Liste - " + currentSubjectName);

        studentsTasksTable = findViewById(R.id.students_tasks_table);

        studentsEntriesAdapter = new StudentsEntriesAdapter(this, currentClass, currentList);

        //1. context
        //2. anzahl spalten; hier die anzahl der schüler currentClass.getNumberOfStudents()
        //3. ausrichtung des lyouts; hier HORIZONTAL was nicht standard ist und vertikal entspricht
        //3. wie die daten angeordnet werden anfang-ende false, ende-anfang true
        studentsTasksTable.setLayoutManager(new GridLayoutManager(this, currentClass.getNumberOfStudents() + 1, LinearLayoutManager.HORIZONTAL, false));

        studentsTasksTable.setAdapter(studentsEntriesAdapter);

        studentsTasksTable.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL));
        studentsTasksTable.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputDialog().show();
            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();
        contentManager.writeData();
    }

    private AlertDialog inputDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View inputDialogView = LayoutInflater.from(this).inflate(R.layout.input_dialog,(ViewGroup) findViewById(android.R.id.content), false);
        final EditText inputDialogEditText = inputDialogView.findViewById(R.id.input_dialog_edit_text);
        inputDialogEditText.setHint(R.string.add_task_input_hint);
        builder.setView(inputDialogView);
        builder.setPositiveButton("Hinzufügen", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogBox, int id){
                if(inputDialogEditText.getText().toString().matches("") == false){
                    addTaskToList(inputDialogEditText.getText().toString());
                }
                dialogBox.dismiss();
            }
        });
        builder.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogBox, int id){
                dialogBox.cancel();
            }
        });

        return builder.create();
    }

    private void addTaskToList(String name_){
        if(currentList.addNewEntry(name_) == -1){
            Toast.makeText(this, "Konnte nicht hinzugefügt werden.\nevtl. selber Name vorhanden.", Toast.LENGTH_LONG).show();
            return;
        }
        studentsEntriesAdapter.dataChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.menu_info:
                Toast.makeText(this, "Langes Drücken auf einen Eintrag in der ersten Zeile öffnet weitere Optionen.", Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
