package com.list.seclist;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import java.io.File;;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class ContentManager extends Application{
    private File file;
    private String fileName;
    private static boolean dataChanged;
    private TreeMap<String, Class> classes;

    public ContentManager(){
        fileName = "classManager";
        dataChanged = false;
        classes = new TreeMap<>();
    }

    public void manageFile(){
        File tmpFile = this.getFileStreamPath(fileName);
        if(tmpFile == null || tmpFile.exists() == false){
            //neu erstellen
            file = new File(this.getFilesDir(), fileName);
        }
        else{
            //nicht neu erstellen
            file = tmpFile;
        }
    }

    private void readData(){
        ArrayList<Class> tmpClasses = Encrypter.decryptRead(this, fileName);
        if(tmpClasses == null){
            classes = new TreeMap<>();
        }
        if(tmpClasses != null){
            for(Class c : tmpClasses){
                if(c != null){
                    addExistingClass(c);
                }
            }
        }
    }
    public void writeData(){
        if(dataChanged == false){
            return;
        }
        ArrayList<Class> tmpClasses = new ArrayList<>();

        for(Map.Entry<String, Class> entry : classes.entrySet()){
            tmpClasses.add(entry.getValue());
        }

        Encrypter.encryptWrite(this, fileName, tmpClasses);
    }

    public Class getClass(String className){
        if(className != null){
            return classes.get(className);
        }
        else{
            return null;
        }
    }
    public ArrayList<String> getClassNames(){
        ArrayList<String> tmpNames = new ArrayList<>();
        for(String name : classes.keySet()){
            tmpNames.add(name);
        }
        return tmpNames;
    }

    public void addNewClass(String name_){
        if(name_ != null){
            Class tmpClass = new Class(name_);
            classes.put(name_, tmpClass);
            ContentManager.dataChanged();
        }
    }
    public void addExistingClass(Class class_){
        if(class_ != null){
            classes.put(class_.getName(), class_);
        }
    }

    public void deleteClass(String name_){
        try{
            this.classes.remove(name_);
            ContentManager.dataChanged();
        }
        catch(Exception ignore){
        }
    }
    public int renameClass(String previousName, String newName_){
        if(newName_ != null && previousName != null){
            if(classes.containsKey(newName_) == false){
                Class tmpClass = classes.remove(previousName);
                tmpClass.setName(newName_);
                classes.put(newName_, tmpClass);
                return 0;
            }
        }
        return -1;
    }

    public int checkPassword(String pw){
        SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        if(sharedPref.contains("pwsalt") && sharedPref.contains("pwiv") && sharedPref.contains("pwencryptedtext")){
            //salt lesen
            byte[] salt = Base64.decode(sharedPref.getString("pwsalt", "error"), Base64.NO_WRAP);
            //iv lesen
            byte[] iv = Base64.decode(sharedPref.getString("pwiv", "error"), Base64.NO_WRAP);
            //verschlüsselten text lesen
            byte[] encryptedtext = Base64.decode(sharedPref.getString("pwencryptedtext", "error"), Base64.NO_WRAP);

            //text mit passwort entschlüsseln
            byte[] decryptedtext = Encrypter.decryptPw(salt, iv, pw, encryptedtext);
            //String decryptedtext = Base64.encodeToString(Encrypter.decryptPw(salt, iv, pw, encryptedtext), Base64.NO_WRAP);
            //wenn passwort richtig war dann daten entschlüsseln
            //überprüfung ob richtig, in dem der entschlüsselte text 'SecList' sein muss
            if(decryptedtext != null){
                if(new String(decryptedtext).equals("SecList")){
                    //erfolgsfall
                    Encrypter.putPw(this, pw);
                    //manageFile();
                    readData();
                    return 0;
                }
            }
            return -1;
        }
        return -2;
    }
    public int setPassword(String pw){
        SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        try{
            Encrypter.secureWipeFile(file);
        }
        catch(Exception e){
            e.printStackTrace();
            return -1;
        }
        try{
            editor.clear();
            editor.commit();
            SharedPreferences sharedPrefSaltIv = this.getSharedPreferences("SaltAndIv", Context.MODE_PRIVATE);
            SharedPreferences.Editor editorSaltIv = sharedPrefSaltIv.edit();
            editorSaltIv.clear();
            editorSaltIv.commit();
        }
        catch(Exception e){
            e.printStackTrace();
            return -1;
        }

        //neues salt
        byte[] salt = Encrypter.getSalt();
        SecureRandom random = new SecureRandom();
        random.nextBytes(salt);
        editor.putString("pwsalt", Base64.encodeToString(salt, Base64.NO_WRAP));
        editor.commit();

        //neues iv
        byte[] iv = Encrypter.getIv();
        SecureRandom ivRandom = new SecureRandom();
        ivRandom.nextBytes(iv);
        editor.putString("pwiv", Base64.encodeToString(iv, Base64.NO_WRAP));
        editor.commit();

        //text 'SecList' mit passwort verschlüsseln und speichern
        byte[] encryptPwText = Encrypter.encryptPw(salt, iv, pw, ("SecList").getBytes());
        if(encryptPwText == null){
            return -1;
        }
        editor.putString("pwencryptedtext", Base64.encodeToString(encryptPwText, Base64.NO_WRAP));
        editor.commit();

        return 0;
    }

    public static void dataChanged(){
        dataChanged = true;
    }
}
