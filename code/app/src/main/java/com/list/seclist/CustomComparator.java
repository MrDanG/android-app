package com.list.seclist;
import java.util.Comparator;

public class CustomComparator {

    public CustomComparator(){

    }

    public StringArrayListComparator getStringArrayListComparator(){
        return new StringArrayListComparator();
    }

    public StudentArrayListComparator getStudentArrayListComparator(){
        return new StudentArrayListComparator();
    }


    private class StringArrayListComparator implements Comparator<String>{
        @Override
        public int compare(String s1, String s2){
            return s1.compareToIgnoreCase(s2);
        }
    }

    private class StudentArrayListComparator implements Comparator<Student>{
        @Override
        public int compare(Student s1, Student s2){
            return s1.getName().compareToIgnoreCase(s2.getName());
        }
    }

}
