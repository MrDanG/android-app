package com.list.seclist;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.security.SecureRandom;
import java.util.ArrayList;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SealedObject;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public abstract class Encrypter{
    private static byte[] salt;
    private static byte[] iv;
    private static byte[] pwSalt;
    private static byte[] pwIv;
    protected static int rndNumber;

    public static byte[] encryptPw(byte[] salt, byte[] iv, String pw, byte[] text){
        try{
            //password in char[] umwandeln
            char[] passwordChar = pw.toCharArray();
            //key erzeugen
            PBEKeySpec pbeKeySpec = new PBEKeySpec(passwordChar, salt, 1324, 256);

            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2withHmacSHA1");
            byte[] keyBytes = secretKeyFactory.generateSecret(pbeKeySpec).getEncoded();
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

            IvParameterSpec ivSpec = new IvParameterSpec(iv);

            //entschlüsseln
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

            return cipher.doFinal(text);
        }
        catch(Exception e){
            System.err.println("----------Fehler in encryptPw()----------");
            return null;
        }
    }

    public static byte[] decryptPw(byte[] salt, byte[] iv, String pw, byte[] text){
        try{
            //password in char[] umwandeln
            char[] passwordChar = pw.toCharArray();
            //key erzeugen
            PBEKeySpec pbeKeySpec = new PBEKeySpec(passwordChar, salt, 1324, 256);

            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2withHmacSHA1");
            byte[] keyBytes = secretKeyFactory.generateSecret(pbeKeySpec).getEncoded();
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

            IvParameterSpec ivSpec = new IvParameterSpec(iv);

            //entschlüsseln
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

            return cipher.doFinal(text);
        }
        catch(Exception e){
            System.err.println("----------Fehler in decryptPw()----------");
            e.printStackTrace();
            return null;
        }
    }

    public static void encryptWrite(Context context, String fileName, ArrayList<Class> tmpClasses){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        BufferedOutputStream bos = null;
        CipherOutputStream cos = null;
        try{
            SecureRandom random = new SecureRandom();
            salt = getSalt();
            random.nextBytes(salt);

            //password in char[] umwandeln
            char[] passwordChar = getPw(context).toCharArray();
            //key erzeugen
            PBEKeySpec pbeKeySpec = new PBEKeySpec(passwordChar, salt, 1324, 256);

            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2withHmacSHA1");
            byte[] keyBytes = secretKeyFactory.generateSecret(pbeKeySpec).getEncoded();
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

            SecureRandom ivRandom = new SecureRandom();
            iv = getIv();
            ivRandom.nextBytes(iv);
            IvParameterSpec ivSpec = new IvParameterSpec(iv);

            //verschlüsseln
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

            try{
                fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
                bos = new BufferedOutputStream(fos);
                cos = new CipherOutputStream(bos, cipher);
                oos = new ObjectOutputStream(cos);
                for(Class tmpClass : tmpClasses){
                    SealedObject so = new SealedObject(tmpClass, cipher);
                    oos.writeObject(so);
                }
                oos.flush();
                cos.flush();
                bos.flush();
                fos.flush();
            }
            catch(Exception e){
                System.err.println("----------Fehler beim Schreiben der Daten----------");
                e.printStackTrace();
            }
            finally{
                try{
                    if(oos != null) oos.close();
                    if(cos != null) cos.close();
                    if(bos != null) bos.close();
                    if(fos != null) fos.close();
                    SharedPreferences sharedPref = context.getSharedPreferences("SaltAndIv", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("salt", Base64.encodeToString(salt, Base64.NO_WRAP));
                    editor.putString("iv", Base64.encodeToString(iv, Base64.NO_WRAP));
                    editor.commit();
                }
                catch(Exception e){
                    System.err.println("----------Fehler beim Schließen der Streams----------");
                    e.printStackTrace();
                }
            }
        }
        catch(Exception e){
            System.err.println("----------Fehler in encryptWrite()----------");
        }
    }

    public static ArrayList<Class> decryptRead(Context context, String fileName){
        ArrayList<Class> tmpClasses = new ArrayList<>();
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        BufferedInputStream bis = null;
        CipherInputStream cis = null;
        SharedPreferences sharedPref = context.getSharedPreferences("SaltAndIv", Context.MODE_PRIVATE);
        if(sharedPref.contains("salt") && sharedPref.contains("iv")){
            salt = Base64.decode(sharedPref.getString("salt", "error"), Base64.NO_WRAP);
            iv = Base64.decode(sharedPref.getString("iv", "error"), Base64.NO_WRAP);
        }
        else{
            return null;
        }
        try{
            //password in char[] umwandeln
            char[] passwordChar = getPw(context).toCharArray();

            //key erzeugen
            PBEKeySpec pbeKeySpec = new PBEKeySpec(passwordChar, salt, 1324, 256);

            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2withHmacSHA1");
            byte[] keyBytes = secretKeyFactory.generateSecret(pbeKeySpec).getEncoded();
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

            IvParameterSpec ivSpec = new IvParameterSpec(iv);

            //entschlüsseln
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            try{
                fis = context.openFileInput(fileName);
                bis = new BufferedInputStream(fis);
                cis = new CipherInputStream(bis, cipher);
                ois = new ObjectInputStream(cis);
                while(true){
                    SealedObject so = (SealedObject) ois.readObject();
                    Class tmpClass = (Class) so.getObject(cipher);
                    if(tmpClass != null){
                        tmpClasses.add(tmpClass);
                    }
                }
            }
            catch(java.io.EOFException eof){
            }
            catch(Exception e){
                System.err.println("----------Fehler beim Lesen der Daten----------");
                e.printStackTrace();
                return null;
            }
            finally{
                try{
                    if(ois != null) ois.close();
                    if(cis != null) cis.close();
                    if(bis != null) bis.close();
                    if(fis != null) fis.close();
                }
                catch(Exception e){
                    System.err.println("----------Fehler beim Schließen der Streams----------");
                    e.printStackTrace();
                    return null;
                }
            }
        }
        catch(Exception e){
            System.err.println("----------Fehler in decryptRead()----------");
            e.printStackTrace();
            return null;
        }
        return tmpClasses;
    }

    public static void secureWipeFile(File file) throws IOException{
        //überschreibt eine Datei mit zufallszahlen und löscht diese
        if(file != null){
            final long length = file.length();
            if(length > 0){
                final SecureRandom random = new SecureRandom();
                final RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rws");
                randomAccessFile.seek(0);
                randomAccessFile.getFilePointer();
                byte data[] = new byte[64];
                int pos = 0;
                while(pos < length){
                    random.nextBytes(data);
                    randomAccessFile.write(data);
                    pos += data.length;
                }
                randomAccessFile.close();
            }
            file.delete();
        }
    }

    public static void putPw(Context context, String pw){
        try{
            pwSalt = getSalt();
            pwIv = getIv();
            SecureRandom random = new SecureRandom();
            random.nextBytes(pwSalt);

            //password in char[] umwandeln
            rndNumber = random.nextInt();
            char[] passwordChar = Integer.toString(rndNumber).toCharArray();
            //key erzeugen
            PBEKeySpec pbeKeySpec = new PBEKeySpec(passwordChar, pwSalt, 1324, 256);

            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2withHmacSHA1");
            byte[] keyBytes = secretKeyFactory.generateSecret(pbeKeySpec).getEncoded();
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

            SecureRandom ivRandom = new SecureRandom();
            ivRandom.nextBytes(pwIv);
            IvParameterSpec ivSpec = new IvParameterSpec(pwIv);

            //verswchlüsseln
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

            byte[] pwEncrypted = cipher.doFinal(pw.getBytes());

            SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("pwencrypted", Base64.encodeToString(pwEncrypted, Base64.NO_WRAP));
            editor.commit();
        }
        catch(Exception e){
            System.err.println("----------Fehler in putPw()----------");
            e.printStackTrace();
        }
    }

    private static String getPw(Context context){
        try{
            //password in char[] umwandeln
            char[] passwordChar = Integer.toString(rndNumber).toCharArray();
            //key erzeugen
            PBEKeySpec pbeKeySpec = new PBEKeySpec(passwordChar, pwSalt, 1324, 256);

            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2withHmacSHA1");
            byte[] keyBytes = secretKeyFactory.generateSecret(pbeKeySpec).getEncoded();
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

            IvParameterSpec ivSpec = new IvParameterSpec(pwIv);

            //entschlüsseln
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

            SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            byte[] pw = Base64.decode(sharedPref.getString("pwencrypted", "error"), Base64.NO_WRAP);

            if(new String(cipher.doFinal(pw)).equals("error")){
                System.err.println("----------Fehler in SharedPreferences getPw() ----------");
                return null;
            }
            else{
                return new String(cipher.doFinal(pw));
            }
        }
        catch(Exception e){
            System.err.println("----------Fehler in getPw()----------");
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] getSalt(){
        byte salt[] = new byte[256];
        return salt;
    }

    public static byte[] getIv(){
        byte[] iv = new byte[16];
        return iv;
    }
}
