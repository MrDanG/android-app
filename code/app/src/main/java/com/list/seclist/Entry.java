package com.list.seclist;

import java.io.Serializable;
import java.util.TreeMap;

public class Entry implements Serializable{
    private String name;
    private TreeMap<String, String> status;
    private String comment;

    public Entry(String name_){
        name = name_;
        status = new TreeMap<>();
        ContentManager.dataChanged();
    }

    public void setStatus(String name_, String state_){
        status.put(name_, state_);
        ContentManager.dataChanged();
    }
    public String getStatus(String name_){
        if(status.containsKey(name_)){
            return status.get(name_);
        }
        else{
            return null;
        }
    }
    public void deleteStatus(String name_){
        try{
            this.status.remove(name_);
            ContentManager.dataChanged();
        }
        catch(Exception e){
        }
    }

    //get/set name
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
        ContentManager.dataChanged();
    }

    //get/set comment
    public String getComment(){
        return comment;
    }
    public void setComment(String comment){
        this.comment = comment;
        ContentManager.dataChanged();
    }
}

