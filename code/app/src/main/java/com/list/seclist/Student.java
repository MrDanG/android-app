package com.list.seclist;

import java.io.Serializable;

public class Student implements Serializable {
    private String name;

    public Student(String name_){
        name = name_;
        ContentManager.dataChanged();
    }

    public String getName(){
        return name;
    }
    public void setName(String name_){
        this.name = name_;
        ContentManager.dataChanged();
    }
}
