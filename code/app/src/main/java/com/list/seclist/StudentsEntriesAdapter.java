package com.list.seclist;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class StudentsEntriesAdapter extends RecyclerView.Adapter<StudentsEntriesAdapter.ViewHolder>{
    private static Context context;
    private static Activity relatedActivity;
    private static Class currentClass;
    private static List currentList;
    private static ArrayList<String> data;

    public StudentsEntriesAdapter(Activity activity_, Class class_, List list_){
        currentClass = class_;
        currentList = list_;
        context = activity_;
        relatedActivity = activity_;
        data = new ArrayList<>();

        prepareData();
    }

    public void dataChanged(){
        data = new ArrayList<>();
        prepareData();
        this.notifyDataSetChanged();
    }

    private void prepareData(){
        /*
         * die einzelnen Listen müssen so aufbereitet werden dass
         * sie in dem recyclerView dargestellt werden können
         * und auf clicks reagiert werden kann
         * die namen der studenten und die aufgaben
         * werden in entsprechender reihenfolge in data kopiert
         */
        for (Object s : currentClass.getStudents()){
            Student student = (Student) s;
            data.add(student.getName());
        }
        //erstes element ist leer
        data.add(0, null);

        for (Object t : currentList.getEntries()){
            Entry entry = (Entry) t;
            data.add(entry.getName());
            for (Object s : currentClass.getStudents()){
                Student student = (Student) s;
                data.add(entry.getStatus(student.getName()));
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        protected TextView tv;
        protected ImageView ivDone;
        protected ImageView ivNotDone;
        protected ImageView ivForgotFetchedLater;
        protected ImageView ivExcused;

        private String[] states = {"Erledigt", "Nicht erledigt", "Vergessen und nachgetragen", "Entschuldigt"};
        private AlertDialog.Builder dialogState = new AlertDialog.Builder(context);
        private String selectedState;

        private String[] statesDelete = {"Löschen", "Umbenennen"};
        private AlertDialog.Builder dialogDelete = new AlertDialog.Builder(context);
        private String selectedDelete;
        private String selectedItem;

        private int clickPos;
        private int taskNumber;
        private int studentNumber;

        public ViewHolder(View v){
            super(v);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
            tv = v.findViewById(R.id.table_item_text);
            ivDone = v.findViewById(R.id.table_item_image_done);
            ivNotDone = v.findViewById(R.id.table_item_image_not_done);
            ivForgotFetchedLater = v.findViewById(R.id.table_item_image_done_right_not_done);
            ivExcused = v.findViewById(R.id.table_item_image_excused);

            dialogState.setItems(states, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which){
                    selectedState = states[which];
                    clickAction();
                }
            });

            dialogDelete.setItems(statesDelete, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which){
                    selectedDelete = statesDelete[which];
                    deleteAction();
                }
            });
        }

        @Override
        public void onClick(View view){
            selectedState = null;

            clickPos = getAdapterPosition();
            taskNumber = clickPos / (currentClass.getNumberOfStudents() + 1);
            studentNumber = (clickPos - taskNumber) % currentClass.getNumberOfStudents();
            if(studentNumber == 0){
                //entspricht dem letzten schüler in der liste
                //rechnung ergibt in dem fall 0
                studentNumber = currentClass.getNumberOfStudents();
            }

            if(studentNumber >= 1 &&
                    //nicht die erste Spalte
                    clickPos > currentClass.getNumberOfStudents() &&
                    //nicht die überschrift folgender spalten
                    clickPos % (currentClass.getNumberOfStudents() + 1) != 0){
                dialogState.show();
            }
        }

        public boolean onLongClick(View view){
            clickPos = getAdapterPosition();
            //enspricht den überschriften der splaten
            if(clickPos % (currentClass.getNumberOfStudents() + 1) == 0 && clickPos != 0){
                selectedItem = data.get(clickPos);
                dialogDelete.show();
            }
            return true;
        }

        private void deleteAction(){
            if(selectedDelete.equals("Löschen") && selectedItem != null){
                currentList.deleteEntry(selectedItem);
                for(int i = 0; i <= currentClass.getNumberOfStudents(); i++){
                    data.remove(clickPos);
                }
                StudentsEntriesAdapter.this.notifyDataSetChanged();
            }
            else if(selectedDelete.equals("Umbenennen") && selectedItem != null){
                inputDialog().show();
            }
        }

        private void clickAction(){
            String studentName = data.get(studentNumber);
            //taskNumber - 1 da nich von 1 bis x sinder von 0 bis x-1
            currentList.changeEntry((taskNumber - 1), studentName, selectedState);
            if (selectedState != null){
                data.set(clickPos, selectedState);
            }
            StudentsEntriesAdapter.this.notifyDataSetChanged();
        }

        private AlertDialog inputDialog(){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            View inputDialogView = LayoutInflater.from(context).inflate(R.layout.input_dialog,(ViewGroup) relatedActivity.findViewById(android.R.id.content), false);
            final EditText inputDialogEditText = inputDialogView.findViewById(R.id.input_dialog_edit_text);
            inputDialogEditText.setHint(R.string.add_task_input_hint);
            builder.setView(inputDialogView);
            builder.setPositiveButton("Umbenennen", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialogBox, int id){
                    if(inputDialogEditText.getText().toString().matches("") == false){
                        StudentsEntriesAdapter.this.notifyDataSetChanged();
                        try{
                            if(currentList.renameEntry(selectedItem, inputDialogEditText.getText().toString()) == -1){
                                Toast.makeText(relatedActivity, "Konnte nicht geändert werden.\nevtl. selber Name vorhanden.", Toast.LENGTH_LONG).show();
                                return;
                            }
                            int pos = data.indexOf(selectedItem);//subjectNamesAdapter.getPosition(selectedItem);
                            data.remove(selectedItem);//subjectNamesAdapter.remove(selectedItem);
                            data.add(pos, inputDialogEditText.getText().toString());//subjectNamesAdapter.insert(, pos);
                        }
                        catch (Exception e){
                        }
                    }
                    dialogBox.dismiss();
                }
            });
            builder.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialogBox, int id){
                    dialogBox.cancel();
                }
            });

            return builder.create();
        }
    }

    @Override
    public StudentsEntriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.table_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        if(position < data.size()){
            String tmpString = data.get(position);

            holder.tv.setVisibility(View.GONE);
            holder.ivDone.setVisibility(View.GONE);
            holder.ivNotDone.setVisibility(View.GONE);
            holder.ivForgotFetchedLater.setVisibility(View.GONE);
            holder.ivExcused.setVisibility(View.GONE);

            if(tmpString == null){
                holder.tv.setVisibility(View.VISIBLE);
                holder.tv.setText(data.get(position));
            }
            else{
                if(tmpString.equals("Erledigt")){
                    holder.ivDone.setVisibility(View.VISIBLE);
                }
                else if(tmpString.equals("Nicht erledigt")){
                    holder.ivNotDone.setVisibility(View.VISIBLE);
                }
                else if(tmpString.equals("Vergessen und nachgetragen")){
                    holder.ivNotDone.setVisibility(View.VISIBLE);
                    holder.ivForgotFetchedLater.setVisibility(View.VISIBLE);
                }
                else if(tmpString.equals("Entschuldigt")){
                    holder.ivExcused.setVisibility(View.VISIBLE);
                }
                else{
                    holder.tv.setVisibility(View.VISIBLE);
                    holder.tv.setText(data.get(position));
                }
            }
        }
    }

    @Override
    public int getItemCount(){
        return data.size();
    }
}