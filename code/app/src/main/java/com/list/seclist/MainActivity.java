package com.list.seclist;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{
    private ContentManager contentManager;

    private int attempts;
    private boolean setNewPassword;

    private Button signIn;
    private Button abort;

    private TextView hint;
    private TextView newPassword;

    private EditText passwordInput;
    private EditText newPasswordInput;
    private EditText repeatNewPasswordInput;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        contentManager = (ContentManager) getApplicationContext();
        contentManager.manageFile();

        setNewPassword = false;

        signIn = findViewById(R.id.login_button);
        signIn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(setNewPassword == true){
                    setPassword();
                }
                else{
                    login();
                }
                clearFields();
            }
        });

        newPassword = findViewById(R.id.login_new_textView);
        newPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNewPassword = true;
                showNewPassword();
                clearFields();
            }
        });

        abort = findViewById(R.id.login_abort_button);
        abort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNewPassword = false;
                showLogin();
                clearFields();
            }
        });

        passwordInput = findViewById(R.id.login_password_edit_text);
        newPasswordInput = findViewById(R.id.login_new_password_edit_text);
        repeatNewPasswordInput = findViewById(R.id.login_repeat_new_password_edit_text);

        hint = findViewById(R.id.login_new_password_hint);
    }


    /**
     * Wird ausgeführt wenn der "signIn-Button" angeklickt wird und die Variable "setNewPassword" false ist.
     */
    private void login(){
        //passwort richtig
        if(contentManager.checkPassword(passwordInput.getText().toString()) == 0){
            final Intent i = new Intent(this, ClassActivity.class);
            startActivity(i);
        }
        //noch kein passwort
        else if(contentManager.checkPassword(passwordInput.getText().toString()) == -2){
            Toast.makeText(this, "Noch kein Passwort angelegt. Bitte anlegen.", Toast.LENGTH_LONG).show();
            return;
        }
        //passwort falsch
        else{
            Toast.makeText(this, "Falsches Passwort", Toast.LENGTH_LONG).show();
            return;
        }
    }

    /**
     * Wird ausgeführt wenn der "signIn-Button" angeklickt wird und die Variable "setNewPassword" true ist.
     */
    private void setPassword(){
        //zweimal gleiches passwort eingegeben und passwort nicht leer
        if(newPasswordInput.getText().toString().equals(repeatNewPasswordInput.getText().toString()) &&
                newPasswordInput.getText().toString().matches("") == false){
            if(contentManager.setPassword(newPasswordInput.getText().toString()) != 0){
            }
            showLogin();
            setNewPassword = false;
        }
        else{
            Toast.makeText(this, "Passwort unterscheidet sich oder ist leer", Toast.LENGTH_LONG).show();
            return;
        }
    }

    /**
     * Ändert die angezeigten Elemente.
     * Es werden die Elemente angezeigt, die zum login nötig sind.
     * Die Methode wird in "onCreate" und "setPassword", wenn ein neues Passwort fetgelegt, oder der Vorgang abgebrochen wird, aufgrufen.
     * Außerdem wird sie aufgerufen, wenn der "Back-Button" angeklickt wird,
     * während die Elemente zum festlegen eines neuen Passwortes angezeigt werden.
     * Dies entspricht einem Abbruch des Vorgangs.
     */
    private void showLogin(){
        //inputfeld für neues passwort unsichtbar
        newPasswordInput.setVisibility(View.GONE);
        //inputfeld für wiederholung des neuen passworts unsichtabr
        repeatNewPasswordInput.setVisibility(View.GONE);
        //hinweis zu neuem passwort unsichtbar
        //hint.setVisibility(View.GONE);

        //abbrechen button unsichtbar
        abort.setVisibility(View.GONE);
        //text des button ändern
        signIn.setText(R.string.login_button_sign_in_text);
        //hinweis text ändern zu login
        hint.setText(R.string.login_hint);

        //inputfeld für passwort sichtbar
        passwordInput.setVisibility(View.VISIBLE);
        //textfeld "neues password festlegen" sichtbar
        newPassword.setVisibility(View.VISIBLE);
    }

    /**
     * Ändert die angezeigten Elemente.
     * Es werden die Elemente angezeigt, die zum Festlegen eines neuen Passwortes nötig sind.
     * Die Methode wird aufgerufen, wenn der "newPassword-TextView" angeklickt wird.
     */
    private void showNewPassword(){
        //textfeld "neues password festlegen" unsichtbar
        newPassword.setVisibility(View.GONE);
        //inputfeld für passwort unsichtbar
        passwordInput.setVisibility(View.GONE);

        //inputfeld für neues passwort sichtbar
        newPasswordInput.setVisibility(View.VISIBLE);
        //inputfeld für wiederholung des neuen passworts sichtbar
        repeatNewPasswordInput.setVisibility(View.VISIBLE);
        //hinweis zu neuem passwort sichtbar
        //hint.setVisibility(View.VISIBLE);

        //abbrechen button sichtbar
        abort.setVisibility(View.VISIBLE);
        //text des button ändern
        signIn.setText("Anwenden");
        //hinweis text ändern zu neuem passwort
        hint.setText(R.string.new_password_hint);
    }

    /**
     * Diese Methode wird aufgrufen, wenn sich die angezeigten Elemente ändern.
     * Die EditText-Elemente werden je nach Wert der Variablen "setNewPassword" anders verwendet.
     * Um diese zu leeren, wird die Methode aufgerufen.
     */
    private void clearFields(){
        passwordInput.getText().clear();
        newPasswordInput.getText().clear();
        repeatNewPasswordInput.getText().clear();
    }

    /**
     * Wenn der "Back-Button" angeklickt wird und die Variable "setNewPassword" true ist,
     * wird die Methode "showLogin()" aufgerufen.
     * Dies ist nötig, da sonst beim Anklicken des "Back-Button" die App geschlossen wird.
     * Ist die Variable "setNewPassword" false, wird die standard Aktion ausgeführt und die App geschlossen.
     */
    @Override
    public void onBackPressed(){
        if(setNewPassword == true){
            setNewPassword = false;
            showLogin();
            clearFields();
        }
        else{
            super.onBackPressed();
        }
    }
}
