package com.list.seclist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class TabStudents extends Fragment implements AdapterView.OnItemLongClickListener{
    private ListView studentsNames;
    private ArrayAdapter<String> studentNamesAdapter;

    private ListStudentActivity relatedActivity;
    private Class currentClass;

    private String[] states = {"Löschen", "Umbenennen"};
    private AlertDialog.Builder dialog;
    private String selectedState;
    private String selectedItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.tab_student, container, false);

        if(getActivity() instanceof ListStudentActivity){
            relatedActivity = (ListStudentActivity) getActivity();
            currentClass = relatedActivity.getCurrentClass();
        }

        //ListView
        studentsNames = view.findViewById(R.id.students_names_ListView);
        //ArrayAdapter initialisieren. Daten aus entsprechender Klasse lesen
        studentNamesAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1,
                currentClass.getStudentsNames());
        //Adapter und ClickListener zuweisen
        studentsNames.setAdapter(studentNamesAdapter);
        studentsNames.setOnItemLongClickListener(this);

        dialog = new AlertDialog.Builder(relatedActivity);
        dialog.setItems(states, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                selectedState = states[which];
                longClickAction();
            }
        });

        return view;
    }

    private void longClickAction(){
        if(selectedState.equals("Löschen") && selectedItem != null){
            currentClass.deleteStudent(selectedItem);
            studentNamesAdapter.remove(selectedItem);
        }
        else if(selectedState.equals("Umbenennen") && selectedItem != null){
            inputDialog().show();
        }
    }

    private AlertDialog inputDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(relatedActivity);
        View inputDialogView = LayoutInflater.from(relatedActivity).inflate(R.layout.input_dialog,(ViewGroup) relatedActivity.findViewById(android.R.id.content), false);
        final EditText inputDialogEditText = inputDialogView.findViewById(R.id.input_dialog_edit_text);
        inputDialogEditText.setHint(R.string.add_student_input_hint);
        builder.setView(inputDialogView);
        builder.setPositiveButton("Umbenennen", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogBox, int id){
                if(inputDialogEditText.getText().toString().matches("") == false){
                    studentNamesAdapter.setNotifyOnChange(true);
                    try{
                        if(currentClass.renameStudent(selectedItem, inputDialogEditText.getText().toString()) == -1){
                            Toast.makeText(relatedActivity, "Konnte nicht geändert werden.\nevtl. selber Name vorhanden.", Toast.LENGTH_LONG).show();
                            return;
                        }
                        int pos = studentNamesAdapter.getPosition(selectedItem);
                        studentNamesAdapter.remove(selectedItem);
                        studentNamesAdapter.insert(inputDialogEditText.getText().toString(), pos);
                    }
                    catch (Exception e){
                    }
                }
                dialogBox.dismiss();
            }
        });
        builder.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogBox, int id){
                dialogBox.cancel();
            }
        });

        return builder.create();
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id){
        selectedItem = (String) studentsNames.getItemAtPosition(pos);
        dialog.show();
        return true;
    }

    public void studentsChanged(String name_){
        studentNamesAdapter.setNotifyOnChange(true);
        if(name_ != null){
            studentNamesAdapter.add(name_);
            //sortieren
            studentNamesAdapter.sort(new CustomComparator().getStringArrayListComparator());
        }
    }
}
