package com.list.seclist;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class TabAdapter extends FragmentStatePagerAdapter{
    private TabLists tabLists;
    private TabStudents tabStudents;

    private Context context;

    //anzahl der tabs
    int tabCount;

    public TabAdapter(FragmentManager fm, int tabCount, Context context_){
        super(fm);
        context = context_;

        this.tabCount = tabCount;
        tabLists = new TabLists();
        tabStudents = new TabStudents();
    }

    @Override
    public Fragment getItem(int position) {
        //aktuelles tab zurückgeben
        switch (position) {
            case 0:
                return tabLists;
            case 1:
                return tabStudents;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getText(R.string.tab_1_text);
            case 1:
                return context.getText(R.string.tab_2_text);
            default:
                return null;
        }
    }

    public void addList(String name_){
        tabLists.ListsChanged(name_);
    }

    public void addStudent(String name_){
        tabStudents.studentsChanged(name_);
    }
}
